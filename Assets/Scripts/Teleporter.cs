﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {

    public int levelID = 0;             //level the teleporter is in
    public int teleporterID = 0;        //id of this teleporter

    public int teleportToLevel = 0;     //level to teleport to
    public int teleportToTeleporter = 0;//teleporter to teleport to
}
