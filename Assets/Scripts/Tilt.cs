﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilt : MonoBehaviour
{
    private bool gyroEnabled;           //if the gyroscope is enabled
    private Gyroscope gyro;             //reference to the gyroscope

    private GameObject boardContainer;  //created to hold the controller
    public Transform board;             //reference to the board

    public float maxTilt;               //maximum angle the board can tilt
    public float tiltSensitivity;       //how much to tilt the board compared to phone tilt
    [HideInInspector]
    public bool upsideDown = false;

    void Start()
    {
        boardContainer = new GameObject("BoardContainer");      //creates the board container
        boardContainer.transform.position = transform.position; //puts it at the controllers postion
        transform.SetParent(boardContainer.transform);          //sets it as the controllers parent

        if (gyroEnabled = EnableGyro()) //tries to enable the gyroscope
            Debug.Log("Gyro enabled!");
        else
            Debug.LogError("Failed to enable gyro");
    }

    private bool EnableGyro()   //used to enable the gyroscope
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            
            board.rotation = Quaternion.Euler(-90, 0, 0);                       //rotates the board and the container  
            boardContainer.transform.rotation = Quaternion.Euler(90f, 0f, 0f);  //- to correct for coordinate system misalignment

            return true;
        }
        return false;
    }

    void FixedUpdate()
    {
        if (gyroEnabled)
        {
            Vector3 newRotation = (gyro.attitude * new Quaternion(0, 0, 1, 0)).eulerAngles; //get the phones rotation from the gyroscope and make them euler angles
            
            if (newRotation.x > 180) newRotation.x -= 360;  //make the angles go from -180 to 180
            if (newRotation.y > 180) newRotation.y -= 360;  //- instead of 0 to 360
            
            //apply the rotation locally
            transform.localRotation = Quaternion.Euler(Mathf.Clamp(newRotation.x / tiltSensitivity, -maxTilt, maxTilt), Mathf.Clamp(newRotation.y / tiltSensitivity, -maxTilt, maxTilt), newRotation.z);

            upsideDown = (Mathf.Abs(newRotation.x) > 90 || Mathf.Abs(newRotation.y) > 90);
        }
    }
}
