﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpBox : MonoBehaviour {

    public string text; //text in popup

	// Use this for initialization
	void Start ()
    {
        Time.timeScale = 0;     //pauses game
        transform.tag = "PopUp";//set own tag
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))   //if back pressed, resume
        {
            Time.timeScale = 1;
            Destroy(gameObject);
        }
    }

    private void OnGUI()
    {
        //scale variables
        float s = Screen.height / 1080f;    //scale
        float wc = Screen.width / 2f;       //horizontal center
        float hc = Screen.height / 2f;      //vertical center
        float w = 300;                      //width of popup
        float h = 75;                       //height of popup

        //textbox
        GUIStyle style = new GUIStyle(GUI.skin.box);
        style.wordWrap = true;
        style.fontSize = (int)(Screen.height * 0.03f);
        style.alignment = TextAnchor.MiddleCenter;
        GUI.contentColor = Color.white;
        GUI.backgroundColor = new Color(.4f, .2f, 0f, 1f);
        GUI.Box(new Rect(wc - s * w, hc - s * h, s * w * 2, s * h * 2), text, style);

        //okay button
        GUIStyle style2 = new GUIStyle(GUI.skin.button);
        style2.fontSize = (int)(Screen.height * 0.03f);
        GUI.contentColor = Color.white;
        GUI.backgroundColor = new Color(.6f, .4f, .2f, 1f);
        if (GUI.Button(new Rect(wc-s*w/4f, hc-s*h/10f + s*h*1.1f, s*w*2/4f, s*w*2/10f), "Okay", style2))
        {   //resume
            Time.timeScale = 1;
            Destroy(gameObject);
        }
    }
}
