﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//key "power up"
public class GateKey : pickUp {

    public GameObject gate; //gate that the key belongs to

	// Use this for initialization
	void Start ()
    {   //if script starts on the powerup and not the play, change color to red
		if (gameObject.tag != "Player")
            GetComponent<Renderer>().material.color = Color.red;
	}

    public override void Power(Collider player)
    {   //destroys gate when key is picked up, and destroys the key
        Destroy(gate);
        Destroy(gameObject);
    }
}
