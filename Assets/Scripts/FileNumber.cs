﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileNumber : MonoBehaviour
{
    //Stores the fileNumber to be retrieved by the MenuHandler
    public int fileNumber;
}
