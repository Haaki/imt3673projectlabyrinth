﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Basic pick up class
public class pickUp : MonoBehaviour {
    public AudioClip pickUpClip;

    void OnTriggerEnter(Collider other)
    {   //Checks for a player and runs the pick ups power
        if (other.tag == "Player")
        {
            AudioSource audio = other.gameObject.GetComponents<AudioSource>()[1];
            audio.clip = pickUpClip;
            audio.volume = PlayerPrefs.GetFloat("VolumeLevel", 1);
            audio.Play();
            Power(other);
        }
    }

    public virtual void Power(Collider player)
    {   //Destroys pickup, actual functionality set in superclass
        Destroy(this.gameObject);
    }
}
