﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//StoppingPower power up
public class StoppingPower : pickUp {

    public float cooldown, timer;   //cooldown and timer of power
    private float cool, t;          //current cooldown and timer
    bool wasPressed = false;        //if power was used last frame
    public Sprite ringSprite;       //sprite of ring showing timer counting down
    GameObject ring;                //ring showing timer counting down

	// Use this for initialization
	void Start ()
    {   //if script starts on not a player changes color to green 
        if (gameObject.tag != "Player")
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
        else
        {   //else sets up timer and cooldown and ring for showing timer
            cool = 0;
            t = timer;

            ring = new GameObject();
            ring.name = "PowerUp Ring";
            ring.AddComponent<SpriteRenderer>().sprite = ringSprite;
            ring.transform.parent = transform;
            ring.transform.localPosition = Vector3.zero;
            ring.GetComponent<SpriteRenderer>().material.color = Color.green;
            ring.transform.localScale = Vector3.zero;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {   //lets player hover if finger is pressed with a cooldown and timer
		if(this.tag == "Player")
        {   //if player
            cool -= Time.deltaTime;
            //count down cooldown
            if (Input.touchCount == 1)
            {   //if on finger
                if(cool <= 0)
                {   //if cooldown is not active
                    t -= Time.deltaTime;    //count down timer
                    if (t > 0)
                    {   //if timer has not ran out
                        wasPressed = true;
                        Rigidbody r = GetComponent<Rigidbody>();                    //gets rigidbody of player 
                        r.constraints = RigidbodyConstraints.FreezePositionY;       //and freezes y pos
                        r.velocity = new Vector3(r.velocity.x, 0, r.velocity.z);    //and sets y velocity to 0
                        ring.transform.eulerAngles = new Vector3(90, 0, 0);         //shows timer
                        ring.transform.localScale = Vector3.one * (.4f+t/2);
                    }
                    else
                    {   //if timer ran out
                        cool = cooldown;    //sets cooldown
                        t = timer;          //resets timer
                        Rigidbody r = GetComponent<Rigidbody>();
                        r.constraints = RigidbodyConstraints.None;  //removes y pos freeze
                        ring.transform.localScale = Vector3.zero;   //removes ring timer
                    }
                }
            }
            else if(wasPressed)
            {   //if player was pressing last frame but not anymore
                wasPressed = false; //last frame was no longer pressed
                cool = cooldown;    //sets cooldown
                t = timer;          //resets timer
                Rigidbody r = GetComponent<Rigidbody>();
                r.constraints = RigidbodyConstraints.None;  //removes y pos freeze
                ring.transform.localScale = Vector3.zero;   //removes ring timer
            }
        }
	}

    public override void Power(Collider player)
    {   //adds power to player
        player.gameObject.AddComponent<StoppingPower>();
        player.gameObject.GetComponent<StoppingPower>().ringSprite = ringSprite;
        player.gameObject.GetComponent<StoppingPower>().cooldown = cooldown;
        player.gameObject.GetComponent<StoppingPower>().timer = timer;
        GameObject popUp = new GameObject("PopUp");     //tells user how to use power
        popUp.AddComponent<PopUpBox>().text = "Keep your finger on the screen to freeze your vertical position.";
        base.Power(player);
    }

    private void OnDestroy()
    {
        if (this.tag == "Player")
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            for (int i = 0; i < transform.childCount; i++)
                Destroy(transform.GetChild(i).gameObject);
        }
    }
}
