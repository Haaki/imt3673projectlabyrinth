﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//super strength power up
public class SuperStrength : pickUp
{
    private void Start()
    {
        if (gameObject.tag != "Player")
        {
            GetComponent<Renderer>().material.color = new Color(.5f, 0f, .5f, 1f);
        }
    }

    void OnCollisionEnter(Collision other)
    {   //if player collides with a Movable wall lets player move wall
        if (other.collider.tag == "Movable Wall")
        {
            if(this.tag == "Player")
            {
                if (other.collider.gameObject.GetComponent<Rigidbody>() == null)
                    other.collider.gameObject.AddComponent<Rigidbody>();
                Rigidbody r = other.collider.gameObject.GetComponent<Rigidbody>();
                other.collider.gameObject.GetComponent<BoxCollider>().center = Vector3.zero;
                other.collider.gameObject.GetComponent<BoxCollider>().size = Vector3.one;
                r.constraints = RigidbodyConstraints.FreezeRotation;        //freezes rotation of wall
                r.AddForce(new Vector3(this.gameObject.GetComponent<Rigidbody>().velocity.x, 0, this.gameObject.GetComponent<Rigidbody>().velocity.z)); //adds force to wall
            }
        }
    }

    public override void Power(Collider player)
    {   //adds power to player
        player.gameObject.AddComponent<SuperStrength>();
        GameObject popUp = new GameObject("PopUp");
        popUp.AddComponent<PopUpBox>().text = "You have obtained super-strength, and can now move purple blocks with ease.";
        base.Power(player);
    }
}
