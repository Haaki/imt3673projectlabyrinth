﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelNumberSelect : MonoBehaviour {
    public int levelNumber;

    public void SelectLevelFromButton()
    {
        GetComponentInParent<MenuHandler>().SelectLevel(levelNumber);
    }
}
