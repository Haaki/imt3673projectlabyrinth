﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class MenuHandler : MonoBehaviour {
    public GameObject[] menus;          //list of menus
    public GameObject[] levels;         //list of levels

    public GameObject boardController;  //prefab containing the ball, and will contain the board then spawned
    public GameObject cameraRig;        //prefab for the camera rig
    public GameObject levelbutton;      //prefab for populating the level menu

    [HideInInspector]
    public int currentMenu = 0;         //current menu in use
    [HideInInspector]
    public int file = 0;                //current file selected
    [HideInInspector]
    public int selectedLevel = 0;       //current level selected
    public const int maxLevel = 10;     //the number of levels currently in the game, also used to check game-clear status
    
    private int timeout;               //stores the players screen timeout setting

    private bool GP = false;            //if Google play has been activated
    private bool loggingIn = false;     //if the player is looging in


    //IDs of the different leaderboards
    private string[] LeaderboardIDs = {
        GPGSIds.leaderboard_level_1,
        GPGSIds.leaderboard_level_2,
        GPGSIds.leaderboard_level_3,
        GPGSIds.leaderboard_level_4,
        GPGSIds.leaderboard_level_5,
        GPGSIds.leaderboard_level_6,
        GPGSIds.leaderboard_level_7,
        GPGSIds.leaderboard_level_8,
        GPGSIds.leaderboard_level_9,
        GPGSIds.leaderboard_level_10,
    };
    
    //Builds configuration for google play client
    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

    private void Awake()
    {
        //sets game quality setting on start
        int q = PlayerPrefs.GetInt("QualityLevel", 3);
        QualitySettings.SetQualityLevel(q, true);

        for (int i = 0; i < levels.Length; i++)
        {
            GameObject lb = Instantiate(levelbutton, menus[5].transform);
            lb.transform.SetSiblingIndex(i + 1);
            lb.GetComponent<LevelNumberSelect>().levelNumber = i;
            lb.GetComponent<Button>().onClick.AddListener(lb.GetComponent<LevelNumberSelect>().SelectLevelFromButton);
        }
    }

    private void Start()
    {
        timeout = Screen.sleepTimeout;
        Screen.orientation = ScreenOrientation.Portrait;

        SceneManager.sceneLoaded += OnSceneLoaded;
        RefreshFiles();
        DontDestroyOnLoad(gameObject);

        if(!GP)
        {   //actives google play if it hasn't been already
            PlayGamesPlatform.Activate();
            GP = true;
        }
        PlayGamesPlatform.InitializeInstance(config);   //inits google play
        PlayGamesPlatform.DebugLogEnabled = true;       //enables debug

        
    }

    public void Update()
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (Input.GetKeyDown(KeyCode.Escape)) //back button
            if (SceneManager.GetActiveScene().name == "Menu")
                switch (currentMenu)
                {
                    case 0: ExitGame(); break;  //exit game if on main menu
                    case 1: OpenMenu(0); break; //goto main menu
                    case 2: OpenMenu(0); break; //goto main menu
                    case 3: OpenMenu(2); break; //goto load saves menu
                    case 4: ResumeGame(); break;//resumes the game
                    case 5: OpenMenu(2); break; //goto load saves menu
                    case 6: OpenMenu(0); break; //goto main menu
                    default: break;
                }
            else if (SceneManager.GetActiveScene().name == "Game")
            {
                //pauses game and opens the menu
                Time.timeScale = 0;
                OpenMenu(4);
                Screen.sleepTimeout = timeout;
            }
    }

    public void OpenMenu(int i) //opens menu i
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        menus[currentMenu].SetActive(false);
        menus[i].SetActive(true);
        currentMenu = i;
    }

    public void SetQuality(Slider slider) //set quality setting from slider
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        PlayerPrefs.SetInt("QualityLevel", (int)slider.value);
        QualitySettings.SetQualityLevel((int)slider.value, true);
    }

    public void SetVolume(Slider slider) //set volume setting from slider
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        PlayerPrefs.SetFloat("VolumeLevel", slider.value);
    }

    public void CloseMenu() //close currently active menu
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        menus[currentMenu].SetActive(false);
    }

    public void QuitToMainMenu() //in-game menu, quits to main menu
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        Screen.sleepTimeout = timeout;
        SceneManager.LoadScene("Menu");
        Destroy(gameObject);
    }

    public void ResetLevel() //in-game menu, resets current level
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<LevelController>().ResetLevel();
        ResumeGame();
    }

    public void ResumeGame() //in-game menu, resumes game
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        Time.timeScale = 1;
        CloseMenu();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void BackFromMenu() //retursn to main menu or game menu depending on scene
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (SceneManager.GetActiveScene().name == "Menu")
            OpenMenu(0);
        else if (SceneManager.GetActiveScene().name == "Game")
            OpenMenu(4);
    }

    public void RefreshFiles() //sets text on file save slots, and sets menu slider
    {
        //activate all menus
        foreach (GameObject menu in menus)
            menu.SetActive(true);
        
        //set file text
        GameObject[] texts = GameObject.FindGameObjectsWithTag("SaveFile");
        foreach (GameObject text in texts)
        {
            int lv = PlayerPrefs.GetInt("CurrentLevel" + text.GetComponentInChildren<FileNumber>().fileNumber, 0);
            text.GetComponentInChildren<Text>().text = "File " 
                + text.GetComponentInChildren<FileNumber>().fileNumber + " | " 
                + ((lv==maxLevel)?"CLEARED":("level "+(lv+1).ToString()));
        }

        //set slider value
        menus[1].GetComponentInChildren<Slider>().value = PlayerPrefs.GetInt("QualityLevel", 3);    //TODO! make this more robust

        //hide all menus
        foreach (GameObject menu in menus)
            menu.SetActive(false);
        //open current menu
        OpenMenu(currentMenu);
    }

    public void SelectLevel(int i) //selects and loads level
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active
        Debug.Log("tried loading level " + i);
        if (i <= PlayerPrefs.GetInt("CurrentLevel" + file, 0)) //if level unlocked
        {
            CloseMenu();
            selectedLevel = i;
            SceneManager.LoadSceneAsync("Game");
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Debug.Log("Loading file " + file);
        }
    }

    public void LoadSaveFile(int f) //loads info from file f
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        file = f;
        OpenMenu(5); //opens level select menu

        GameObject[] texts = GameObject.FindGameObjectsWithTag("LevelSelect");

        for (int i = 0; i < levels.Length; i++)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(PlayerPrefs.GetFloat("Level" + i + "Timer" + file, 59 * 60f + 59.999f));
            string timeText = string.Format("{0:D2}:{1:D2}.{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
            
        }

        //sets text on levels
        for (int i=0; i<texts.Length; i++)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(PlayerPrefs.GetFloat("Level" + i + "Timer" + file, 59*60f+59.999f));
            string timeText = string.Format("{0:D2}:{1:D2}.{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);

            texts[i].GetComponentInChildren<Text>().text = "Level " + (i+1) + " | " + timeText;

            //sets color based on whether the level has been unlocked
            if (i <= PlayerPrefs.GetInt("CurrentLevel" + file, 0)) //if level unclocked
            {
                texts[i].GetComponentInChildren<Text>().color = Color.black;
                texts[i].GetComponent<Button>().interactable = true;
            }
            else
            {
                texts[i].GetComponentInChildren<Text>().color = new Color(0.25f, 0.25f, 0.25f, 1);
                texts[i].GetComponent<Button>().interactable = false;
            }
        }
    }

    public void DeleteSaveFile(int f) //delete locally saved data
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        //reset data
        PlayerPrefs.SetInt("CurrentLevel" + f, 0);
        PlayerPrefs.SetInt("CurrentTeleporter" + f, 0);
        for (int i = 0; i < maxLevel; i++)
            PlayerPrefs.SetFloat("Level" + i + "Timer" + f, 59 * 60f + 59.999f);
        PlayerPrefs.SetInt("Deathless" + f, 1); //sets deathless run to true
        //refresh data in menus
        RefreshFiles();
        OpenMenu(2);
        GameObject popUp = new GameObject("PopUp");
        popUp.AddComponent<PopUpBox>().text = "Deleted file " + f + "!";
    }

    public void LoadLeaderboard()
    {   //loads leaderboard if user is logged in to google play
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (Social.localUser.authenticated) //if user is logged in
            Social.ShowLeaderboardUI();     //shows leaderboard
        else
        {   //tells user they're not logged in
            GameObject popUp = new GameObject("PopUp");
            popUp.AddComponent<PopUpBox>().text = "Not logged in";
        }
    }

    public void RefreshUser()
    {   //opens the social menu and sets log in/out text
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        OpenMenu(6);
        if (Social.localUser.authenticated)
            GameObject.Find("Social 1").GetComponentInChildren<Text>().text = "Log out";
        else
            GameObject.Find("Social 1").GetComponentInChildren<Text>().text = "Log in";
    }

    public void LogUser()
    {   //logs user in or out
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (Social.localUser.authenticated)
            LogOut();
        else
            LogIn();
    }

    public void LogIn()
    {   //logs user in
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0 || loggingIn)
            return; //menu can not be interacted with while a popup is active

        loggingIn = true;
        Social.localUser.Authenticate((bool success) =>
        {   //tries to authenticate user
            if (success)
            {   //if succesfully authenticated
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
                //sets pop ups to bottom

                Social.ReportProgress(GPGSIds.achievement_logged_in, 100.0f, (bool success2) =>
                {   //tries to pop achievement for logging in
                    //if success a popup pops
                });

                GameObject popUp = new GameObject("PopUp"); //tells user they're logged in
                popUp.AddComponent<PopUpBox>().text = "Logged in";
            }
            RefreshUser();
            loggingIn = false;
        });
    }

    public void LogOut()
    {   //logs out user
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        PlayGamesPlatform.Instance.SignOut();
        if (Social.localUser.authenticated)
        {   //if user is still authenticated 
            GameObject popUp = new GameObject("PopUp");
            popUp.AddComponent<PopUpBox>().text = "Not logged out?";
        }
        else
        {   //if user was succesfully logged out
            GameObject popUp = new GameObject("PopUp");
            popUp.AddComponent<PopUpBox>().text = "Logged out";
        }
        RefreshUser();
    }

    public void ShowAchievements()
    {   //loads achievements if player is logged in
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (Social.localUser.authenticated) //if user is logged in
            Social.ShowAchievementsUI();    //shows achievements
        else
        {   //tells user they're not logged in
            GameObject popUp = new GameObject("PopUp");
            popUp.AddComponent<PopUpBox>().text = "Not logged in";
        }
    }

    public void PostScore()
    {   //post all recorded times to leaderboards if user is logged in
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

        if (Social.localUser.authenticated)
        {   //if user is logged in
            bool posted = true; //if all scores are succesfully posted
            for(int i = 0; i < maxLevel; i++)
            {   //goes through all levels
                for(int j = 1; j <=3; j++)
                {   //goes through all files
                    double time = (PlayerPrefs.GetFloat("Level" + i + "Timer" + j, 59 * 60f + 59.999f)); //gets time for level from file
                    Social.ReportScore(Convert.ToInt64(time*1000), LeaderboardIDs[i], (bool success) =>
                    {   //tries to post score to leaderboard
                        if (!success)   //if one fails 
                            posted = false; //one or more has failed to upload
                    });
                }
            }

            GameObject popUp = new GameObject("PopUp");
            if (posted) //tells user all times were posted succesfully
                popUp.AddComponent<PopUpBox>().text = "Scores posted";
            else        //tells user not all times were posted
                popUp.AddComponent<PopUpBox>().text = "All scores not posted";
        }
        else
        {   //tells user they're not logged in
            GameObject popUp = new GameObject("PopUp");
            popUp.AddComponent<PopUpBox>().text = "Not logged in";
        }
       
    }

    public void ExitGame()
    {
        if (GameObject.FindGameObjectsWithTag("PopUp").Length > 0)
            return; //menu can not be interacted with while a popup is active

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode) //loads game scene
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;

        //create player and board
        LevelController lc = Instantiate(boardController, transform.position, transform.rotation).GetComponentInChildren<LevelController>();
        for (int i = 0; i < levels.Length; i++)
        {
            lc.levelPrefabs.Add(levels[i]);
        }
        lc.currentSaveFile = file;
        lc.currentLevelID = selectedLevel;
        lc.currentTeleporter = 0;
        Debug.Log("Current level " + lc.currentLevelID + ", Current teleporter " + lc.currentTeleporter);

        //create camera
        CameraController cc = Instantiate(cameraRig, transform.position, transform.rotation).GetComponent<CameraController>();
        cc.ball = lc.transform;
        cc.boardController = lc.transform.parent;

        Debug.Log("File " + file + " loaded!");
        Time.timeScale = 1;
    }

    private void OnGUI() //Shows a menu button if in editor, used soley for debugging purposes
    {
#if UNITY_EDITOR
        if (SceneManager.GetActiveScene().name == "Game")
        {
            if (GUI.Button(new Rect(5, 20, 50, 20), "Menu"))
            {
                Time.timeScale = 0;
                OpenMenu(4);
            }
        }
#endif
    }
}
