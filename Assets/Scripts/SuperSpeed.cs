﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//super speed power up
public class SuperSpeed : pickUp {

    private int red = 0, green = 0, blue = 0;   //used for changing color

	// Use this for initialization
	void Start ()
    {   //if script starts on player adds acceleration
        if (this.tag == "Player")
        {
            GetComponent<Acceleration>().forceAmplitude *= 2f;
            GetComponent<Acceleration>().maxSpeed *= 2f;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {   //changes color of object
        Color color = Color.black;
        color.r = (float)(red)/255;
        color.g = (float)(green)/255;
        color.b = (float)(blue)/255;

        red++;
        blue += 2;
        green += 5;

        if (red > 255)
            red -= 255;
        if (blue > 255)
            blue -= 255;
        if (green > 255)
            green -= 255;

        GetComponent<Renderer>().materials[0].color = color;
    }

    public override void Power(Collider player)
    {   //gives power to player
        player.gameObject.AddComponent<SuperSpeed>();
        GameObject popUp = new GameObject("PopUp");
        popUp.AddComponent<PopUpBox>().text = "You have obtained super-speed: your speed and acceleration has been doubled.";
        base.Power(player);
    }

    private void OnDestroy()
    {
        if (this.tag == "Player")
        {
            GetComponent<Acceleration>().forceAmplitude /= 2f;
            GetComponent<Acceleration>().maxSpeed /= 2f;
            GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
