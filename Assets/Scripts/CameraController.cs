﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform boardController;   //the transform of the object controlling the board
    public Transform ball;              //the transform of the ball
    public float camLerpSpeed = 10;     //how fast to rotate the camera
    public float camRotSpeed = 10;      //how fast to move after the ball
    
	void FixedUpdate () {
        //the difference between the current y rotation of the camera and the desired y rotation of the camera (the boards z rotation)
        float diff = (boardController.localRotation.eulerAngles.z - (360f - transform.localRotation.eulerAngles.y));

        //lets the angle comparison work in the spot where angles wrap around
        if (diff < -180) diff = (boardController.localRotation.eulerAngles.z - (360f - transform.localRotation.eulerAngles.y) + 360);
        if (diff > 180) diff = (boardController.localRotation.eulerAngles.z - (360f - transform.localRotation.eulerAngles.y) - 360);

        //what the y rotation is after the difference is applied
        float lerped = (360f - transform.localRotation.eulerAngles.y) + diff * Time.deltaTime * camRotSpeed;

        //applying the rotaion
        transform.rotation = Quaternion.Euler(0, -lerped, 0);

        //meanwhile, positon tracking via lerp is easy 
        transform.position += (ball.position - transform.position) * Time.deltaTime * camLerpSpeed;
    }
}
