﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Enlarge power up
public class Enlarge : pickUp {

    public float enlarge;   //the factor it should be enlarged by
    private Vector3 size;   //normal size

    void Start()
    {   
        if (tag == "Player")    //when script starts on a player stores the normal size
            size = GetComponent<Transform>().localScale;
        else                    //else it sets the powerups color to dark yellow / brown
            GetComponent<Renderer>().material.color = new Color(.3f, .2f, 0, 1f);
    }

    // Update is called once per frame
    void Update()
    {   //enlarges player based on pinching on screen
        if (Input.touchCount == 2)
        {   //if two fingers on the screen
            if (this.tag == "Player")
            {
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                if (deltaMagnitudeDiff > 1)
                {   //if pinch in
                    Transform t = GetComponent<Transform>();
                    //if not normal size
                    if (t.localScale.x > size.x)
                        //set normal size
                        t.localScale = size;
                }
                else if (deltaMagnitudeDiff < -1)
                {   //if pinch out
                    Transform t = GetComponent<Transform>();
                    //if not enlarged
                    if (t.localScale.x < size.x * enlarge)
                        //enlarge
                        t.localScale = size * enlarge;
                }
            }
        }
    }

    public override void Power(Collider player)
    {   //adds power to player
        player.gameObject.AddComponent<Enlarge>();
        player.gameObject.GetComponent<Enlarge>().enlarge = enlarge;
        GameObject popUp = new GameObject("PopUp");
        popUp.AddComponent<PopUpBox>().text = "'Zoom' in and out using your fingers to shrink and enlarge the ball.";
        base.Power(player);
    }

    private void OnDestroy()
    {
        //resets the ball size when the powerup is removed
        if (this.tag == "Player")
            transform.localScale = size;
    }
}

