﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    Vector3 position;                               //base position
    float move = 0;                                 //variable for setting position
    public Vector3 magnitude = new Vector3(1,0,0);  //direction and magnitude of movement
    public float speed = 1;                         //speed of movement (movement speed is independent of distance)
    public float offset = 0;                        //time offset

	// Use this for initialization
	void Start ()
    {
        position = transform.localPosition;         //save initial position
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //move platform
        move = Mathf.Cos(Time.timeSinceLevelLoad * speed + offset) * 2;
        transform.localPosition = position + new Vector3(
            Mathf.Clamp(move*magnitude.x, -magnitude.x, magnitude.x), 
            Mathf.Clamp(move*magnitude.y, -magnitude.y, magnitude.y), 
            Mathf.Clamp(move*magnitude.z, -magnitude.z, magnitude.z));
	}
}
