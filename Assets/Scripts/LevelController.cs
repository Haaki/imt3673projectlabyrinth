﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class LevelController : MonoBehaviour {

    private Rigidbody rb;                   //rigidbody of the player (ball)

    public AudioClip teleportSoundClip;
    private AudioSource teleporSound;

    public GameObject board;                //the game board
    public List<GameObject> levelPrefabs;   //level prefabs, used to load in levels dynamically

    public GameObject currentLevel;         //the current level object
    public int currentLevelID = 0;          //id for the current level
    public int currentTeleporter = 0;       //id for the current teleporter
    public int currentSaveFile;             //id for the current save file

    public bool justTeleported = false;     //whether the ball has just teleported, 
                                            //  ensures that the ball doesn't instantly teleport when reaching its destination

    public bool deathless = true;           //If the game is deathless or not

    public float timer = 0;                 //time used in the current level

    //IDs of level achievements
    private string[] achievementIDs = {
        GPGSIds.achievement_level_1,
        GPGSIds.achievement_level_2,
        GPGSIds.achievement_level_3,
        GPGSIds.achievement_level_4,
        GPGSIds.achievement_level_5,
        GPGSIds.achievement_level_6,
        GPGSIds.achievement_level_7,
        GPGSIds.achievement_level_8,
        GPGSIds.achievement_level_9,
        GPGSIds.achievement_level_10,
    };

    //builds config for google play client
    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        //create current level
        currentLevel = Instantiate(levelPrefabs[currentLevelID]);
        Vector3 rotation = currentLevel.transform.localEulerAngles;
        currentLevel.transform.parent = board.transform;
        currentLevel.transform.localEulerAngles = rotation;
        currentLevel.transform.localPosition = Vector3.zero;

        //loads the player into the last saved teleporter
        justTeleported = true;
        timer = 0;
        LoadLevel(currentLevelID, currentTeleporter);

        //inits google play
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        teleporSound = gameObject.GetComponents<AudioSource>()[1];
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
	}

    public void LoadLevel(int level, int teleporter) //used to teleport to a teleporter, load new levels, and save progress
    {
        //if teleporting to a different level
        if (currentLevelID != level)
        {
            //level score
            if (PlayerPrefs.GetInt("CurrentLevel" + currentSaveFile, 0) < level)    //if the player reaches a later level than in the saved data
            {
                PlayerPrefs.SetInt("CurrentLevel" + currentSaveFile, level);
                Social.ReportProgress(achievementIDs[currentLevelID], 100.0f, (bool success) =>
                {   //unlocks achievement for completeing level
                        //pop up pops if success
                });
                if(level == MenuHandler.maxLevel)
                {
                    Social.ReportProgress(GPGSIds.achievement_you_did_it, 100.0f, (bool success) =>
                    {   //unlocks achievement for completeing game
                        //pop up pops if success
                    });
                    PlayGamesPlatform.Instance.IncrementAchievement(GPGSIds.achievement_are_you_bored, 1, (bool success) => 
                    {
                        //pops achievement when progress is done
                    });
                    PlayGamesPlatform.Instance.IncrementAchievement(GPGSIds.achievement_wow_you_are_really_bored, 1, (bool success) =>
                    {
                        //pops achievement when progress is done
                    });

                    if(PlayerPrefs.GetInt("Deathless" + currentSaveFile, 1) != 0)
                    {   //Should not be default unless it's the first game after download I think
                        Social.ReportProgress(GPGSIds.achievement_deathless, 100.0f, (bool success) =>
                        {
                            //pops achiement on success
                        });
                    }
                }
            }

            //timer score
            if (teleporter == 0 && timer > 0)   //if you teleport to the first teleporter in the room (i.e. cleared a level)
            {
                Debug.Log("Saved Time: " + PlayerPrefs.GetFloat("Level" + currentLevelID + "Timer" + currentSaveFile, 59 * 60f + 59.999f));
                Debug.Log("Current Time: " + timer);
                if (PlayerPrefs.GetFloat("Level" + currentLevelID + "Timer" + currentSaveFile, 59 * 60f + 59.999f) > timer)
                {
                    PlayerPrefs.SetFloat("Level" + currentLevelID + "Timer" + currentSaveFile, timer);
                }
                timer = 0;
            }

            //destroy the current level object, and load a new one
            Destroy(currentLevel);
            if (levelPrefabs.Count <= level) //if final level has been cleared
            {
                level = 0;
                teleporter = 0;
                GameObject popUp = new GameObject("PopUp");
                popUp.AddComponent<PopUpBox>().text = "You have cleared the final level!";
            }
            currentLevel = Instantiate(levelPrefabs[level]);
            Vector3 rotation = currentLevel.transform.localEulerAngles;
            currentLevel.transform.parent = board.transform;
            currentLevel.transform.localEulerAngles = rotation;
            
            currentLevelID = level;


            //Clear powerups
            if (GetComponent<StoppingPower>())
                Destroy(GetComponent<StoppingPower>());
            if (GetComponent<SuperSpeed>())
                Destroy(GetComponent<SuperSpeed>());
            if (GetComponent<SuperStrength>())
                Destroy(GetComponent<SuperStrength>());
            if (GetComponent<Shrink>())
                Destroy(GetComponent<Shrink>());
            if (GetComponent<Enlarge>())
                Destroy(GetComponent<Enlarge>());
        }

        //saves the last teleporter, functionality is currently deprecated
        currentTeleporter = teleporter;
        PlayerPrefs.SetInt("CurrentTeleporter" + currentSaveFile, teleporter);

        //set position to assigned teleporter
        GameObject[] teleporters = GameObject.FindGameObjectsWithTag("Teleporter");
        for (int i = 0; i < teleporters.Length; i++)
            if (teleporters[i].GetComponent<Teleporter>().levelID == level && teleporters[i].GetComponent<Teleporter>().teleporterID == teleporter)
                transform.position = teleporters[i].transform.position;

        //reset velocity
        rb.velocity = Vector3.zero;
    }

    void OnTriggerEnter(Collider other)
    {
        //hit teleporter
        if (other.tag == "Teleporter")
            if (!(other.gameObject.GetComponent<Teleporter>().levelID == 0 && other.gameObject.GetComponent<Teleporter>().teleporterID == 0))
                if (justTeleported == false)
                {
                    int level = other.gameObject.GetComponent<Teleporter>().teleportToLevel;

                    if (level >= currentLevelID) //teleport if it goes to the same or greater level
                    {
                        justTeleported = true;
                        int teleporter = other.gameObject.GetComponent<Teleporter>().teleportToTeleporter;
                        LoadLevel(level, teleporter);teleporSound.clip = teleportSoundClip;
                        teleporSound.volume = PlayerPrefs.GetFloat("VolumeLevel", 1)*0.5f;
                        teleporSound.Play();
                    }
                }

        //hit death zone/object
        if (other.tag == "Death")
        {
            justTeleported = true;
            LoadLevel(currentLevelID, currentTeleporter);
            PlayerPrefs.SetInt("Deathless" + currentSaveFile, 0);   //sets deathless run to false
            teleporSound.clip = teleportSoundClip;
            teleporSound.volume = PlayerPrefs.GetFloat("VolumeLevel", 1) * 0.5f;
            teleporSound.Play();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Teleporter") //left teleporter collider
        {
            justTeleported = false;
        }
    }

    public void ResetLevel()
    {
        justTeleported = true;
        timer = 0;
        int level = currentLevelID;
        int teleporter = 0;
        currentLevelID = 999;
        currentTeleporter = 0;
        LoadLevel(level, teleporter);
    }

    private void OnGUI()
    {
        //draws the timer on-screen
        GUIStyle style = new GUIStyle(GUI.skin.label);
        style.fontSize = (int)(Screen.height * 0.03f);
        GUI.contentColor = Color.white;

        TimeSpan timeSpan = TimeSpan.FromSeconds(timer);
        string timeText = string.Format("{0:D2}:{1:D2}:{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
        GUI.Label(new Rect(0, 0, Screen.width*.7f, Screen.height*.1f), "Time: " + timeText, style);
    }
}
