﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//shrink power up
public class Shrink : pickUp {
    public float shrink;    //factor to shrink by

    private Vector3 size;   //normal size

	// Use this for initialization
	void Start ()
    {   
        if (tag == "Player")//if script starts on player stores normal size
            size = GetComponent<Transform>().localScale;
        else    //else set color of pickup to yellow
            GetComponent<Renderer>().material.color = Color.yellow;
    }

    // Update is called once per frame
    void Update()
    {   //shrinks player based on pinching on screen
        if (Input.touchCount == 2)
        {   //if two fingers on screen
            if (this.tag == "Player")
            {   //if player
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                if (deltaMagnitudeDiff > 1)
                {   //if pinch in
                    Transform t = GetComponent<Transform>();
                    //if not already shrunk
                    if (t.localScale.x > size.x*shrink)
                        //shrink
                        t.localScale = size * shrink;
                }
                else if (deltaMagnitudeDiff < -1)
                {   //if pinch out
                    Transform t = GetComponent<Transform>();
                    //if not already normal size
                    if (t.localScale.x < size.x)
                        //set normal size
                        t.localScale = size;
                }
            }
        }
    }

    public override void Power(Collider player)
    {   //adds power to player
        player.gameObject.AddComponent<Shrink>();
        player.gameObject.GetComponent<Shrink>().shrink = shrink;
        GameObject popUp = new GameObject("PopUp"); //tells user how to use power
        popUp.AddComponent<PopUpBox>().text = "'Zoom' in and out using your fingers to shrink and enlarge the ball.";
        base.Power(player);
    }

    private void OnDestroy()
    {
        //resets the ball size when the powerup is removed
        if (this.tag == "Player")
            transform.localScale = size;
    }
}
