﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acceleration : MonoBehaviour
{
    private Rigidbody rb;   //the balls rigidbody
    private AudioSource au; //the balls audio source

    private Vector3 forceVector;            //wich way to push the ball
    public float forceAmplitude = 10000;    //how hard to push the ball
    public float jumpHeight = 20;           //multiplier for jumping
    public float maxSpeed = 50;     //universal speed limit of steel marbles
    public bool isSindre = false;   //if we're making fun of Sindre

    private Vector3 lastVel = new Vector3(0,0,0);
    private Vector3 deltaVel;

    private void Start()
    {
        //get references to the components
        rb = GetComponent<Rigidbody>();
        au = GetComponent<AudioSource>();
    }

    void FixedUpdate ()
    {
        forceVector = Input.acceleration;   //get data from the accelerometer

        //aligns it with the rotation of the board
        if (isSindre) forceVector = Quaternion.Euler(90, 0, 0) * forceVector;
        else forceVector = Quaternion.Euler(transform.parent.localRotation.eulerAngles.x + 90, transform.parent.localRotation.eulerAngles.y, transform.parent.localRotation.eulerAngles.z) * forceVector;

        if (!GetComponentInParent<Tilt>().upsideDown) forceVector.y *= -1;  //aligns gravity

        if (forceVector.magnitude > 1.5f)   //  allows jumping
        {
            forceVector.y *= jumpHeight;
            forceVector.y = Mathf.Min(forceVector.y, 0);
        }
        forceVector.y = Mathf.Min(forceVector.y, -1);   //makes jumping easier

        rb.AddForce(forceVector * forceAmplitude);  //applies force

        if (rb.velocity.magnitude > maxSpeed)   //enforces speed limit
            rb.velocity = rb.velocity.normalized * maxSpeed;
        deltaVel = rb.velocity - lastVel;
        lastVel = rb.velocity;
	}

    private void OnCollisionEnter(Collision collision)  //called when a collision happens
    {
        au.volume = PlayerPrefs.GetFloat("VolumeLevel", 1) * (lastVel - rb.velocity).magnitude / maxSpeed;
        au.Play();
    }
}
